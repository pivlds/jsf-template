#jsf-template

An eclipse project template for running JSF + CDI on Tomcat7

## Target Environment

### Server Libraries

* Tomcat7 
* JSF 2.2 
* CDI 1.2

### IDE Format

** Eclipse : javaee-mars **

To resolve errors after initial import:

* In Eclipse:
* Project Explorer > Right-click project node
* Maven > Update Project 

## References

Based on this article:

https://musingsinjava.wordpress.com/2014/11/02/enabling-jsf-2-2-and-cdi-1-2-on-tomcat-8/